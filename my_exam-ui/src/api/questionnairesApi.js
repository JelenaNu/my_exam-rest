import HTTP from '.'

export default {
    fetchMyQuestionnaires() {
        return HTTP.get('/myquestionnaires')
    },
    fetchMyExams() {
        return HTTP.get('/exams')
    },
    saveQuestionnaire(questionnaire) {
        return HTTP.post('/questionnaire', questionnaire)
    },
    fetchQuestionnaireById(id) {
        return HTTP.get(`/questionnaire/${id}`)
    },
    deleteQuestionnaireById(id) {
        HTTP.get(`/questionnaire/${id}/delete`)
    },
    deleteQuestionById(id) {
        HTTP.get(`/question/${id}/delete`)
    },
    saveQuestion(question) {
        return HTTP.post('/question', question)
    },
    fetchQuestionById(id) {
        return HTTP.get(`/question/${id}`)
    },
    fetchMyProfile() {
        return HTTP.get('/myprofile')
    },
    saveOption(option) {
        return HTTP.post('/option', option)
    }, 
    deleteOptionById(id) {
        HTTP.get(`/option/${id}/delete`)
    },
    fetchOptionById(id) {
        return HTTP.get(`option/${id}`)
    },
    fetchPublicQuestionnaires() {
        return HTTP.get('/publicquestionnaires')
    },
    fetchQuestionsForExam(questionnaireId) {
        return HTTP.get(`/exam/questions/${questionnaireId}`)
    },
    takeExam(selectedOptions) {
        return HTTP.post('/exam', selectedOptions)
    }
   
}