import HTTP from '.'

export default {
    getUser() {
        return HTTP.get('/user')
    },
    updateUser(user, file) {
        let data = new FormData()
        data.append("file", file)
        data.append("username", user.username)
        data.append("firstName", user.firstName)
        data.append("lastName", user.lastName)
        data.append("info", user.info)
        return HTTP.post('/user/update', data)
    },
    fetchMyProfile() {
        return HTTP.get("/myprofile")
    }

}