import React from 'react'
import './questionnaire.css'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'

export default (props) => {    

    const { t } = useTranslation("common") 

    return (
            <div className="card-container">
                <div className = "card">
                    {
                        props.selfLink ? 
                            <Link to={`/questionnaire/${props.questionnaire.id}`}>
                                <h3>{props.questionnaire.theme}</h3>
                            </Link>
                        : <h3>{props.questionnaire.theme}</h3>
                    }
                    <div className="action-container">
                        <Link to={`/questionnaire/${props.questionnaire.id}/edit`}>
                            <div className="mint-button "dangerouslySetInnerHTML={{__html: "&#9998;"}} />
                        </Link>
                        {/* <Link to="">
                            <div className="mint-button "dangerouslySetInnerHTML={{__html: "&#128065;"}} />
                        </Link> */}
                        <Link to={`/questionnaire/${props.questionnaire.id}/delete`}>
                            <div className="mint-button" dangerouslySetInnerHTML={{__html: "&#128465;"}} />
                        </Link>
                    </div>
                    <hr></hr>            
                    <div className="grid">
                        <div><span className="prop-name">{t("language")} </span><br></br>{props.questionnaire.language}</div>
                        <div><span className="prop-name">{t("createdon")} </span><br></br>{new Date(props.questionnaire.creationDate).toLocaleDateString()}</div>
                        <div><span className="prop-name">{t("accessibility")} </span><br></br>{props.questionnaire.accessibility}</div>                
                        <div><span className="prop-name">{t("passedtimes")} </span><br></br>{props.questionnaire.passedExams}</div>
                        <div><span className="prop-name">{t("rightanswers")} % </span><br></br>{props.questionnaire.rightAnswerPercentage}</div>
                    </div>
                    <div className="description">
                        {props.questionnaire.description}
                    </div>            
                </div>                
                
            </div>    
    )
}