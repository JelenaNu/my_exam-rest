import React from 'react'
import { Formik, Form, Field } from 'formik'
import * as Yup from 'yup'
import ErrorMessageTranslated from '../ErrorMessageTranslated/ErrorMessageTranslated'
import '../../validation'
import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router-dom'
import questionnaireApi from '../../api/questionnairesApi'

const validationSchema = Yup.object().shape({
    questionText: Yup.string()
        .label("common:text")
        .required()
})

export default ({question}) => {

    const { t } = useTranslation("common")
    const history = useHistory()

    return (            
        <div className="form-card">
            <h3>{t("question")}</h3>
            <br></br>
            <Formik
                initialValues={ {
                    id: question.id,
                    questionnaireId: question.questionnaireId,
                    questionText: question.questionText}
                } 
                validationSchema={validationSchema}
                onSubmit={values => {
                    questionnaireApi.saveQuestion(values)
                    history.push(`/questionnaire/${question.questionnaireId}`)
                }}>    
                
                    {() => (
                        <Form>
                            <Field type="hidden" name="id" />
                            <Field type="hidden" name="questionnaireId" />
                            <div>
                                <p><label htmlFor="questionText">{t("text")}</label></p>
                                <Field as="textarea" name="questionText" className="textarea" />
                                <ErrorMessageTranslated className="error" name="questionText" />
                            </div>
                            <br></br>
                            <div>
                                <input type="submit" value={t("save")} className="button"></input>
                            </div>
                        </Form>
                    )}       
            </Formik>
        </div>
    )
}