import React from 'react'
import Option from '../Option'
import { Link } from 'react-router-dom'

export default ({question}) => {
    return (
        <>
            <div className="action-container">
                <Link to={`/question/${question.id}/edit`}>
                    <div className="mint-button " dangerouslySetInnerHTML={{__html: "&#9998;"}} />
                </Link>
                <Link to={`/question/${question.id}/delete`}>
                    <div className="mint-button " dangerouslySetInnerHTML={{__html: "&#128465;"}} />
                </Link>
            </div>

            <div>  { 
                question ? 
                    <>         
                        <>
                            {question.questionText}
                            <ol className="option-list">
                            {
                                question.options.map( (option) => (<li key={option.id}><Option option={option}/></li>))
                            }
                            </ol>
                        </>
                    </> : ""
            }
            <br></br>
            </div>
        </>
    )
}