import React from 'react'
import { Redirect, Switch, Route } from 'react-router-dom'
import WelcomePage from '../../pages/WelcomePage/WelcomePage'
import './content.css'
import QuestionnairesList from '../../pages/QuestionnairesList/QuestionnairesList'
import ExamPage from '../../pages/ExamPage'
import QuestionnairePage from '../../pages/QuestionnairePage/QuestionnairePage'
import AddQuestionnairePage from '../../pages/AddQuestionnairePage/AddQuestionnairePage'
import EditQuestionnairePage from '../../pages/EditQuestionnairePage/EditQuestionnairePage'
import DeleteQuestionnairePage from '../../pages/DeleteQuestionnairePage/DeleteQuestionnairePage'
import EditQuestionPage from '../../pages/EditQuestionPage/EditQuestionPage'
import AddQuestionPage from '../../pages/AddQuestionPage/AddQuestionPage'
import EditProfilePage from '../../pages/EditProfilePage/EditProfilePage'
import DeleteQuestionPage from '../../pages/DeleteQuestionPage/DeleteQuestionPage'
import AddOptionPage from '../../pages/AddOptionPage/AddOptionPage'
import EditOptionPage from '../../pages/EditOptionPage/EditOptionPage'
import QuestionnairesForExamList from '../../pages/QuestionnairesForExamList/QuestionnairesForExamList'
import ExamStatisticsPage from '../../pages/ExamStatisticsPage/ExamStatisticsPage'


export default () => {
    return (
        <div id="content">
            
            <div id="content-container">
                <Switch>
                    <Redirect exact from="/" to="/main" />

                    <Route path="/exam/questionnaire/:id">
                        <ExamPage />
                    </Route>

                    <Route path="/option/:id/edit">
                        <EditOptionPage />
                    </Route>

                    <Route path="/question/:id/edit">
                        <EditQuestionPage />
                    </Route>

                    <Route path="/question/:id/delete">
                        <DeleteQuestionPage />
                    </Route>

                    <Route path="/questionnaire/:id/addquestion">
                        <AddQuestionPage />
                    </Route>

                    <Route path="/question/:id/addoption">
                        <AddOptionPage />
                    </Route>
                    
                    <Route path="/main">
                        <WelcomePage />
                    </Route>

                    <Route path="/profile/edit">
                        <EditProfilePage />
                    </Route>

                    <Route path="/questionnaire/:id/edit">
                        <EditQuestionnairePage />
                    </Route>
                    

                    <Route path="/questionnaire/:id/delete">
                        <DeleteQuestionnairePage />
                    </Route>

                    <Route path="/questionnaire/:id">
                        <QuestionnairePage />
                    </Route>

                    <Route path="/myquestionnaires/addnew">
                        <AddQuestionnairePage />
                    </Route>

                    <Route path="/myquestionnaires">
                        <QuestionnairesList />
                    </Route>

                    <Route path="/myexams">
                        <ExamStatisticsPage />
                    </Route>                    
                    
                    <Route path="/publicquestionnaires">
                        <QuestionnairesForExamList />
                    </Route> 

                </Switch>
            </div>
        </div>
    )
}