import React from 'react'
import questionnaireApi from '../../api/questionnairesApi'
import { Formik, Form, Field } from 'formik'
import * as Yup from 'yup'
import ErrorMessageTranslated from '../ErrorMessageTranslated/ErrorMessageTranslated'
import '../../validation'
import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router-dom'

const validationSchema = Yup.object().shape({
    theme: Yup.string()
        .label("common:theme")
        .required(),
    description: Yup.string()
        .label("common:description")
        .required()
})

export default ({questionnaire}) => {

    const { t } = useTranslation("common")
    const history = useHistory()

    return (            
        <div className="form-card">
            <h3>{t("questionnaire")}</h3><br></br>

            <Formik
                initialValues={ {
                    id: questionnaire.id,
                    theme: questionnaire.theme,
                    language: questionnaire.language,
                    description: questionnaire.description,
                    accessibility: questionnaire.accessibility,
                    examCount: questionnaire.examCount,
                    creationDate: questionnaire.creationDate,
                    rightAnswerCount: questionnaire.rightAnswerCount
                }
                } 
                validationSchema={validationSchema}
                onSubmit={values => {
                    console.log(values)
                    questionnaireApi.saveQuestionnaire(values)
                    history.push("/myquestionnaires")
                }}>    
                
                    {() => (
                        <Form>
                            <Field type="hidden" name="id" />
                            <Field type="hidden" name="examCount" />
                            <Field type="hidden" name="rightAnswerCount" />
                            <Field type="hidden" name="creationDate" />
                            <div>
                                <p><label htmlFor="theme">{t("theme")}</label></p>
                                <Field as="textarea" name="theme" type="text"/>
                                <ErrorMessageTranslated className="error" name="theme" />
                            </div>
                            <br></br>
                            <div>
                                <p><label htmlFor="language">{t("language")}</label></p>
                                <Field as="select" name="language" className="textfield">
                                    <option value="english">English</option>
                                    <option value="russian">Русский</option>
                                    <option value="lithuanian">Lietuvių</option>
                                </Field>                                
                            </div>
                            <br></br>
                            <div>
                                <p><label htmlFor="description">{t("description")}</label></p>
                                <Field as="textarea" name="description" className="textarea" maxlength="250"/>
                                <ErrorMessageTranslated className="error" name="description" />
                            </div>
                            <br></br>
                            <div>
                                <p><label htmlFor="accessibility">{t("accessibility")}</label></p>
                                <Field as="select" name="accessibility" className="textfield">  
                                    <option value="private">{t("private")}</option>
                                    <option value="public">{t("public")}</option>
                                </Field>                             
                            </div>
                            <br></br>
                            <div>
                                <input type="submit" value={t("save")} className="button"></input>
                            </div>
                        </Form>

                    )}       
            </Formik>
        </div>
    )
}