import React from 'react'
import { Link } from 'react-router-dom'
import questionnairesApi from '../../api/questionnairesApi'
import { useTranslation } from 'react-i18next'

export default ({option}) => {

    const { t } = useTranslation("common")

    const onClick = (e) => {
        e.preventDefault()
        questionnairesApi.deleteOptionById(option.id);
    }

    return (
        <div>   
            {option.optionText} {option.answer ? <u><small>{t("answer")}</small></u> : ""} 
            <Link to={`/option/${option.id}/edit`}>
                <div className="mint-button "dangerouslySetInnerHTML={{__html: "&#9998;"}} />
            </Link>
        
            <a className="mint-button" href="#"  onClick={onClick}>&#128465;</a>
            <br></br> 
        </div>
    )
}