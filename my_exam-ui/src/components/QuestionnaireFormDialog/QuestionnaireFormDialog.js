import React, { useState } from 'react'
import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    Button
} from '@material-ui/core'
import QuestionnaireForm from '../../components/QuestionnaireForm'

export default ({open, handleClose, questionnaire}) => {
    return (
        <Dialog open={open} onClose={handleClose}>
            <DialogTitle>Saving the Questionnaire</DialogTitle>
            <DialogContent>
                <QuestionnaireForm questionnaire={questionnaire} />
            </DialogContent>
            <DialogActions>
                <Button variant="contained" color="default" onClick={handleClose}>Close</Button>
                <Button  variant="contained" color="default" form="questionnaire-form" type="submit">Save</Button>
            </DialogActions>
        </Dialog>
    )
}