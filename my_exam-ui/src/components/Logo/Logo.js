import React from 'react'
import './logo.css'
import { useTranslation } from 'react-i18next'

export default () => {

    const { t } = useTranslation("common")

    return (
        <div id="logo">
            <div className="logo__text"><h1>my_exam</h1></div>
            &nbsp;            
            <div className="text">{t("questionnaires")}</div>            
        </div>
        
    )
}