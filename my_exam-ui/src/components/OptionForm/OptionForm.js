import React from 'react'
import { Formik, Form, Field } from 'formik'
import * as Yup from 'yup'
import ErrorMessageTranslated from '../ErrorMessageTranslated/ErrorMessageTranslated'
import '../../validation'
import { useTranslation } from 'react-i18next'
import questionnaireApi from '../../api/questionnairesApi'
import { useHistory } from 'react-router-dom'

const validationSchema = Yup.object().shape({
    optionText: Yup.string()
        .label("common:text")
        .required()
})

export default ({option}) => {

    const { t } = useTranslation("common")
    const history = useHistory()

    return (            
        <div className="form-card">
            <h3>{t("option")}</h3>

            <Formik
                initialValues={ {
                    id: option.id,
                    questionId: option.questionId,
                    optionText: option.optionText,
                    answer: option.answer
                }} 
                validationSchema={validationSchema}
                onSubmit={values => {
                    questionnaireApi.saveOption(values)
                    history.push(`/question/${option.questionId}/edit`)

                }}>    
                
                    {() => (
                        <Form>
                            <Field type="hidden" name="id" />
                            <Field type="hidden" name="questionId" />
                            <div>
                                <p><label htmlFor="optionText">{t("text")}</label></p>
                                <Field name="optionText" type="text" className="textfield"/>
                                <ErrorMessageTranslated className="error" name="optionText" />
                            </div>
                            <br></br>
                            <div>
                                <p><label htmlFor="answer">{t("answer")}</label></p>
                                <Field as="select" name="answer" className="textfield">
                                    <option value="true">true</option>
                                    <option value="false">false</option>
                                </Field>                                
                            </div>
                            <br></br>
                            <div>
                                <input type="submit" value={t("save")} className="button"></input>
                            </div>
                        </Form>
                    )}       
            </Formik>
        </div>
    )
}