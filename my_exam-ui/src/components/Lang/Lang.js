import React from 'react'
import './lang.css'
import { useTranslation } from 'react-i18next'

export default () => {

    const { i18n } = useTranslation()

    const changeLanguage = lang => e => {
        e.preventDefault()
        i18n.changeLanguage(lang)
    }

    return (

        <div id="lang">
            <div className="lang">
                <a href="#" onClick={changeLanguage('en')}>EN</a>                
            </div>
            <div className="lang">
                <a href="#" onClick={changeLanguage('ru')}>RU</a>
            </div>
            <div className="lang">
                <a href="#" onClick={changeLanguage('lt')}>LT</a>
            </div>
            
        </div>
    )
}