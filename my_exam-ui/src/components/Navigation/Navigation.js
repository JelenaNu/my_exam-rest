import React from 'react'
import './navigation.css'
import { NavLink } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

export default () => {

    const { t } = useTranslation("common")

    return (
        
        <div>   
            
            <div id="navigation">            
                <ul>
                    <li>
                        <NavLink to="/myquestionnaires" activeClassName="selected" className="navlink">
                            {t("questionnaires")}
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/myexams" activeClassName="selected" className="navlink">
                            {t("exams")}
                        </NavLink>
                    </li>
                    
                </ul>      
            </div>
        </div>        
        
    )
}