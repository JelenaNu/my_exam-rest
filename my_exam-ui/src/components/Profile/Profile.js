import React, { useContext, useEffect, useState } from 'react'
import './profile.css'
import { UserContext } from '../../App'
import { setCredentials } from '../../api'
import { Link } from 'react-router-dom'
import questionnairesApi from '../../api/questionnairesApi'
import { useTranslation } from 'react-i18next'

export default () => {

    const { logout, loggedIn } = useContext(UserContext)
    const [profile, setProfile] = useState({})
    const { t } = useTranslation("common")

    useEffect( () => {
        questionnairesApi.fetchMyProfile()
        .then(response => setProfile(response.data))
    }, [profile])
    
    const logoutClick = (e) => {
        e.preventDefault()
        setCredentials(null)
        logout()
    }

    const loggedInBlock = loggedIn() ? (
        <> {
            profile ? <>
                 <div>
                   <div className="flex">
                        <div>
                                {
                                    profile.profilePicture ? <img src={`http://localhost:8080/pic/${profile.profilePicture}`} alt={t("avatar")} className="profile-photo"/> :
                                    <img src={`http://localhost:8080/pic/anonymous.png`} alt={t("avatar")} className="profile-photo"/>
                                }
                        </div>

                        <div className="user-name">
                            {profile.firstName}<br></br>{profile.lastName}
                        </div>

                        <div className="margin">
                            <Link to="/profile/edit" className="gear">&#9881;</Link>
                        </div>
                        
                    </div>
                   <div className="profile-details">{profile.info}</div>
                   <br></br>
                   <hr></hr>
                   <br></br>
                    
                </div>
            </> : ""
        }
               
    <div className="logout"><a href="#" onClick={logoutClick}>{t("logout")}</a></div>
            
        </>
    ) :
    <Link to="/main">Login</Link>
     
    return (

        <div className="profile">            
            {loggedInBlock}
        </div>

    )
}