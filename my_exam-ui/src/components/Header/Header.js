import React from 'react'
import Logo from '../../components/Logo'
import Lang from '../../components/Lang';

export default () => {
    return (
        <header id="header">
        <Logo />
        <Lang />
    </header>
    )
}