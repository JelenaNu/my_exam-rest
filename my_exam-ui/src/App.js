import React, { useState } from 'react'

import { BrowserRouter as Router } from "react-router-dom";
import Navigation from './components/Navigation';
import Profile from './components/Profile';

import Content from './components/Content';
import Header from './components/Header';
import WelcomePage from './pages/WelcomePage/WelcomePage';


const UserContext = React.createContext(null)

function App() {

  const [user, setUser] = useState(null);

  const userContextState = {
    user,
    login: (user) => setUser(user),
    logout: () => setUser(null),
    loggedIn: () => !!user
  }

  return (

    <UserContext.Provider value={userContextState}>
      <Router>  

        <div id="body-container">
          <Header />
            { 
              userContextState.loggedIn() ? (
                 <div id="main-container">
              <nav id="sidebar">                
                <Profile />
                <Navigation />
              </nav>
              <main id="main">                
                <Content />
              </main>
            </div>
              ) : <WelcomePage />
            }
          
           
         

        </div>        
                
      </Router>
    </UserContext.Provider>
  )
}

export default App
export { UserContext }
