import i18next from 'i18next'
import { initReactI18next } from 'react-i18next'
import languageDetector from 'i18next-browser-languagedetector'

import lt from './translations/lt.json'
import en from './translations/en.json'
import ru from './translations/ru.json'

i18next
    .use(initReactI18next)
    .use(languageDetector)
    .init({
        resources : {
            lt,
            en,
            ru
        },
        load: 'languageOnly',
        ns: ['common'],
        fallbackLng: 'en',
        whitelist: ['lt', 'en', 'ru'],
        debug: true,
        interpolation: {
            escapeValue: false
        }
    })

export default i18next