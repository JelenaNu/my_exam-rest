import React, { useState, useEffect } from 'react'
import { useParams, NavLink } from 'react-router-dom'
import QuestionForm from '../../components/QuestionForm/QuestionForm'
import questionnairesApi from '../../api/questionnairesApi'

export default () => {

    const { id } = useParams();
    const [question, setQuestion] = useState();

    useEffect( () => {
        questionnairesApi.fetchQuestionById(id)
            .then (response => setQuestion(response.data));
    }, [id])

    return (
        <div>
            {
                question ? 
                    <>
                        <QuestionForm question={question}/> 
                        <NavLink to={`/questionnaire/${question.questionnaireId}`} className="link">&#8592; back</NavLink><br></br><br></br>
                        <NavLink to={`/question/${question.id}/addoption`} className="link">&#10010; add new option</NavLink>
                    
                    </>
                
                : ""
            }
            
        
        </div>
        
    )
}