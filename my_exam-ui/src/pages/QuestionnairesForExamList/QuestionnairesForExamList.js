import React, { useEffect, useState, useContext } from 'react'
import questionnairesApi from '../../api/questionnairesApi'
import { NavLink } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { UserContext } from '../../App'

export default () => {
    const { t } = useTranslation("common")
    const [questionnaires, setQuestionnaires] = useState([]);
    const { user } = useContext(UserContext)

    useEffect( () => {
        questionnairesApi.fetchPublicQuestionnaires()
            .then(response => setQuestionnaires(response.data))
    }, [])

    return (
        <div>
            {
                questionnaires ? 
                <div>
                    {
                        questionnaires.map( questionnaire => 
                        <div key={questionnaire.id}>
                            <NavLink to={`/exam/questionnaire/${questionnaire.id}`} className="link">{questionnaire.theme}</NavLink>
                            {
                                user.roles.includes("ADMIN") ?
                                    <NavLink to={`/questionnaire/${questionnaire.id}/delete`}>
                                        <div className="mint-button "dangerouslySetInnerHTML={{__html: "&#128465;"}} />
                                    </NavLink> : ""
                            }                           

                            <p>by {questionnaire.authorsUsername}</p>
                            <p>{t("language")}: {questionnaire.language}; {t("createdon")} {new Date(questionnaire.creationDate).toLocaleDateString()}</p>
                            <p>{questionnaire.description}</p>
                        </div>)
                    }
                </div> : ""
            }
        </div>
    )
}