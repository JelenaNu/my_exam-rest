import React, { useContext, useState } from 'react'
import { UserContext } from '../../App'
import { Formik, Form, Field } from 'formik'
import * as Yup from 'yup'
import ErrorMessageTranslated from '../../components/ErrorMessageTranslated/ErrorMessageTranslated'
import '../../validation'
import { useTranslation } from 'react-i18next'
import userApi from '../../api/userApi'

const validationSchema = Yup.object().shape({
    firstName: Yup.string()
        .label("common:firstName")
        .required(),
    lastName: Yup.string()
        .label("common:lastName")
        .required(),
    info: Yup.string()
        .label("common:info")
        .required()
})

export default () => {

    const { t } = useTranslation("common")
    const { user } = useContext(UserContext)
    const [file, setFile] = useState({})

    const handleFileChange = (e) => {
        setFile(e.target.files[0]);
    }

    return (
        <div>
            <h3>{t("editprofile")}</h3>
            <div>
                {user.login}

                <Formik 
                    initialValues = {{
                        username: user.username,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        info: user.info
                        //avatar:
                        //password:
                    }}
                    validationSchema={validationSchema}
                    onSubmit={values => {
                        userApi.updateUser(values, file)                    
                }}>    
                    
                    {() => (
                        <Form>
                            <Field type="hidden" name="username" />                            
                            <div>
                                <p><label htmlFor="firstName">{t("firstName")}</label></p>
                                <Field name="firstName" type="text" className="textfield" />
                                <ErrorMessageTranslated className="error" name="firstName" />
                            </div>
                            <br></br>
                            <div>
                                <p><label htmlFor="lastName">{t("lastName")}</label></p>
                                <Field name="lastName" type="text" className="textfield" />
                                <ErrorMessageTranslated className="error" name="lastName" />
                            </div>
                            <br></br>
                            <div>
                                <p><label htmlFor="info">{t("info")}</label></p>
                                <Field name="info" as="textarea" />
                                <ErrorMessageTranslated className="error" name="info" />
                            </div>
                            <br></br>
                            <div>
                                <p><label htmlFor="file">{t("file")}</label></p>
                                <Field name="file" type="file" onChange={handleFileChange} />
                            </div>
                            <br></br>
                            <div>
                                <input type="submit" value={t("save")} className="button" ></input>
                            </div>
                        </Form>
                    )}       
                </Formik>
            </div>
        </div>
    )
}