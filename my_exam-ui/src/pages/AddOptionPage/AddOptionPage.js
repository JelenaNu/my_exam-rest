import React from 'react'
import { useParams, NavLink } from 'react-router-dom'
import OptionForm from '../../components/OptionForm/OptionForm'
import { useTranslation } from 'react-i18next'

export default () => {

    const { id } = useParams()
    const { t } = useTranslation("common")

    const option = {
        id: '',
        questionId: id,
        optionText: '',
        answer: false
    }

    return (
        <div>            
            <h3>{t("addnewoption")}</h3>
            <OptionForm option={option}/>   
            <NavLink to={`/question/${option.questionId}/edit`} className="link">&#8592; {t("back")}</NavLink><br></br>    
        </div>        
    )
}