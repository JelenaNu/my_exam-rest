import React from 'react'
import QuestionForm from '../../components/QuestionForm/QuestionForm'
import { useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

export default () => {

    const { id } = useParams()
    const { t } = useTranslation("common")

    const question = {
        id: '',
        questionnaireId: id,
        questionText: ''
    }

    return (
        <div>            
            <h3>{t("addnewquestion")}</h3>
            <QuestionForm question={question}/>         
        </div>        
    )
}