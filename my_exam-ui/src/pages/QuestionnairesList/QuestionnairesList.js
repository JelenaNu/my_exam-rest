import React, { useEffect, useState } from 'react'
import Questionnare from '../../components/Questionnaire'
import questionnairesApi from '../../api/questionnairesApi'
import { NavLink } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

export default () => {

    const { t } = useTranslation("common")
    const [questionnaires, setQuestionnaires] = useState([]);

    useEffect( () => {
        questionnairesApi.fetchMyQuestionnaires()
            .then(response => setQuestionnaires(response.data))
    }, [questionnaires])

   return (
         <div>       
            <div className="page-nav">
                <NavLink to="/myquestionnaires/addnew" className="page-nav">
                    &#10010; <span className="page-nav__item">{t("addnewquestionnaire")}</span>
                </NavLink>
                
            </div> 
           <div className="clear block">
           {
                questionnaires.map(questionnaire => (                   
                    <Questionnare questionnaire={questionnaire} selfLink key={questionnaire.id}/>                                                          
                ))
            }          
            
           </div>
        </div>
        
    )
} 