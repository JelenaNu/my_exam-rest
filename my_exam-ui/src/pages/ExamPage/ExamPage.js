import React, { useState, useEffect } from 'react'
import { useParams, useHistory } from 'react-router-dom'
import questionnairesApi from '../../api/questionnairesApi'
import { Formik, Form, Field } from 'formik'

export default () => {

    const { id } = useParams()
    const [questions, setQuestions] = useState()
    const history = useHistory()

    useEffect( () => {
        questionnairesApi.fetchQuestionsForExam(id)
            .then(response => setQuestions(response.data))
    }, [])

    const initialValues = {}
    
    if(questions != undefined)
        for (let i = 0; i < questions.length; i++) {                
            initialValues["selectedOptionId_" + i] = 0 
        }
    
    const onSubmit = (values) => {
        let selectedOptions = []
        Object.keys(values).map( key => {
            selectedOptions.push(parseInt(values[key]))
        })
        questionnairesApi.takeExam(selectedOptions)
        history.push("/myexams")
    }


    return (
        
       <div>
           {
               Object.keys(initialValues).length == 0 ? "" :
               <Formik
               initialValues={initialValues} 
               onSubmit={ values => onSubmit(values)}>                  
                   {() => (
                       <Form>
                            <ol>
                                {   
                                questions.map( (question, i) =>                             
                                    <li key={question.id}>
                                        {question.questionText}

                                        <ol className="option-list">
                                        {
                                            question.options ? question.options.map( option =>                                
                                                    <li key={option.id}>

                                                        <label htmlFor={"selectedOptionId_" + i}>
                                                            {option.optionText}
                                                        </label>
                                                        <Field type="radio" name={"selectedOptionId_" + i} value={option.id.toString()} />   

                                                    </li>                       
                                                ) : ""
                                        }
                                        </ol>
                                    </li>                           
                                ) }
                            </ol> 

                           <div>
                               <input type="submit" value="submit" className="button"></input>
                           </div>
                       </Form>
   
                   )}       
               </Formik>
           }
       </div>

    )
}