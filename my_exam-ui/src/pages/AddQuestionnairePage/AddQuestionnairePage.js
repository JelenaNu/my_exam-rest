import React from 'react'
import QuestionnaireForm from '../../components/QuestionnaireForm/QuestionnaireForm'
import { useTranslation } from 'react-i18next'

export default () => {

    const { t } = useTranslation("common")

    const questionnaire = {
        id: '',
        theme: '',
        language: 'english',
        description: '',
        accessibility: 'private',
        examCount: 0,
        rightAnswerCount: 0
    }

    return (
        <div>
            <h3>{t("addnewquestionnaire")}</h3>
            <QuestionnaireForm questionnaire={questionnaire} />
        </div>
        
    )
}