import React, { useContext } from 'react'
import { setCredentials } from '../../api'
import { Formik, Form, Field } from 'formik'
import { UserContext } from '../../App'
import { useHistory } from 'react-router-dom'
import userApi from '../../api/userApi'
import './welcome.css'
import ErrorMessageTranslated from '../../components/ErrorMessageTranslated/ErrorMessageTranslated'
import { useTranslation } from 'react-i18next'
import '../../validation'
import * as Yup from 'yup'


const initialValues = {
    username: '',
    password: ''
}

const validationSchema = Yup.object().shape({
    username: Yup.string()
        .label("common:username")
        .required(),
    password: Yup.string()
        .label("common:password")
        .required()
})

export default () => {

    const {login} = useContext(UserContext)
    const history = useHistory();
    const { t } = useTranslation("common")

    const onSubmit = values => {
        setCredentials(values)

        userApi.getUser()
            .then(({data}) => {
                login(data)
                history.push("/myquestionnaires")
            })
    }

    return (
        <div className="white-bg">           

            <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={onSubmit}>
               {
                   () => (
                   <div className="position-center login-form">
                        <Form>
                            <div>
                                <p><label htmlFor="username">{t("username")}: </label></p>
                                <Field name="username" type="text" className="textfield" />
                                <ErrorMessageTranslated className="error" name="username" />
                            </div>
                            <div>
                                <p><label htmlFor="password">{t("password")}: </label></p>
                                <Field name="password" type="password" className="textfield" />
                                <ErrorMessageTranslated className="error" name="password" />
                            </div>
                            <br></br>
                            <div>
                                <button type="submit">{t("submit")}</button>
                            </div>
                        </Form>
                   </div>
                   )
               }

            </Formik>

        </div>
    )
}