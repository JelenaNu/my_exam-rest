import React, { useState, useEffect } from 'react'
import { useParams, NavLink } from 'react-router-dom'
import QuestionnaireForm from '../../components/QuestionnaireForm/QuestionnaireForm'
import questionnairesApi from '../../api/questionnairesApi';
import { useTranslation } from 'react-i18next';

export default () => {

    const { id } = useParams();
    const [questionnaire, setQuestionnaire] = useState()
    const { t } = useTranslation("common")

    useEffect( () => {
        questionnairesApi.fetchQuestionnaireById(id)
            .then (response => setQuestionnaire(response.data));
    }, [id])

    return (
        <div>
            <h3>{t("editquestionnaire")}</h3>
            {questionnaire ? <QuestionnaireForm questionnaire={questionnaire} /> : ""}<br></br>
            <NavLink to="/myquestionnaires" className="link">&#8592; {t("backtoquestionnaires")}</NavLink>
        </div>
        
    )
}