import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import questionnairesApi from '../../api/questionnairesApi';
import Questionnare from '../../components/Questionnaire'
import Question from '../../components/Question'
import { NavLink } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

export default () => {

    const { t } = useTranslation("common")

    const { id } = useParams();
    const [questionnaire, setQuestionnaire] = useState(null);

    useEffect( () => {
        questionnairesApi.fetchQuestionnaireById(id)
            .then (response => setQuestionnaire(response.data));
    }, [questionnaire])

    return (
        <div>
            
             <div className="page-nav">
                <NavLink to={`/questionnaire/${id}/addquestion`} className="page-nav">
    &#10010; <span className="page-nav__item">{t("addnewquestion")}</span>
                </NavLink>                
            </div> 

            <div className="clear block">
                {
                    questionnaire ? (                    
                       <> 
                        <Questionnare questionnaire={questionnaire} key={questionnaire.id}/>
                        <ol className="question-list">
                        {
                            questionnaire.questions.map((question) => (<li key={question.id}><Question question={question}/></li>))
                        }                
                        </ol>       
                       </>         
                ) : ""}
                
            </div>

            <NavLink to="/myquestionnaires" className="link">&#8592; {t("backtoquestionnaires")}</NavLink>

        </div>
    )

}