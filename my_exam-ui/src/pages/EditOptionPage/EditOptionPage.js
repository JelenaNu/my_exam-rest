import React, { useState, useEffect } from 'react'
import { useParams, NavLink } from 'react-router-dom'
import OptionForm from '../../components/OptionForm/OptionForm'
import questionnairesApi from '../../api/questionnairesApi'
import { useTranslation } from 'react-i18next'

export default () => {

    const { id } = useParams()
    const [option, setOption] = useState()
    const { t } = useTranslation("common") 

    useEffect( () => {
        questionnairesApi.fetchOptionById(id)
            .then (response => setOption(response.data));
    }, [id])

    return (
        <div>
            {
                option ? 
                    <>
                        <OptionForm option={option}/> 
                        <NavLink to={`/question/${option.questionId}/edit`} className="link">&#8592; {t("back")}</NavLink><br></br>
                        
                    </>
                
                : ""
            }
            
        
        </div>
        
    )
}