import React, {useEffect, useState} from 'react'
import questionnairesApi from '../../api/questionnairesApi'
import { NavLink } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

export default () => {

    const { t } = useTranslation("common")    
       
    const [exams, setExams] = useState([]);

    useEffect( () => {
        questionnairesApi.fetchMyExams()
            .then(response => setExams(response.data))
    }, [exams])

    return (
        <div>
            
            <div className="page-nav">
                <NavLink to="/publicquestionnaires" className="page-nav"> 
                &#10000; <span className="page-nav__item">{t("takeexam")}</span>
                </NavLink>
                
            </div> 
            
            <br></br>
            <h3 className="center margin__bottom-20 clear">{t("examstatistics")}</h3>
            <table className="margin__auto"> 
                <thead>
                    <tr>
                        <th>№</th>
                        <th>{t("passingdate")}</th>
                        <th>{t("questionnaire")}</th>
                        <th>{t("rightanswers")}</th>
                        <th>%</th>
                    </tr>
                </thead> 
                <tbody>  
                    {exams.map((exam, i) => (
                        <tr key={i + exam.takingDate}>
                            <td>{i + 1}</td>
                            <td>{new Date(exam.takingDate).toLocaleDateString()}</td>
                            <td>{exam.theme} by {exam.authorsFullName}
                                <p><NavLink to={`/exam/questionnaire/${exam.questionnaireId}`} className="link">Take again</NavLink></p>
                            </td>
                            <td>{exam.rightAnswerCount}</td>
                            <td>{exam.answerPercentage}</td>
                        </tr>
                    ))}
                </tbody> 
            </table>
        </div>
    )
}