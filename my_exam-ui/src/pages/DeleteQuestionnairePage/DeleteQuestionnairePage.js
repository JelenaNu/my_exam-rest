import React, { useEffect, useState } from 'react'
import { useParams, NavLink } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import questionnairesApi from '../../api/questionnairesApi'
import { useHistory } from 'react-router-dom'


export default () => {
    const { t } = useTranslation("common") 
    const { id } = useParams()
    const [questionnaire, setQuestionnaire] = useState()
    const history = useHistory()

    useEffect( () => {
        questionnairesApi.fetchQuestionnaireById(id)
            .then (response => setQuestionnaire(response.data));
    }, [id])

    return (
        <div>
            <h3>{t("deletequestionnaire")}</h3>
            {
                questionnaire ? 
                <div className = "card">                            
               
                    <div><span className="prop-name">{t("theme")} </span>
                        {questionnaire.theme}
                    </div>
                    <div><span className="prop-name">{t("language")} </span>
                        {questionnaire.language}
                    </div>
                    <div><span className="prop-name">{t("createdon")} </span>
                        {new Date(questionnaire.creationDate).toLocaleDateString()}
                    </div>
                    <div><span className="prop-name">{t("accessibility")} </span>
                        {questionnaire.accessibility}
                    </div>                
                    <div className="description"><span className="prop-name">{t("description")} </span>
                        {questionnaire.description}
                    </div>  
                 
                <div>
                    <br></br>
                    <button onClick={() => {
                        questionnairesApi.deleteQuestionnaireById(id);
                        history.push("/myquestionnaires")}
                        }
                    >{t("submit")}</button>&nbsp;&nbsp;
                    <NavLink to="/myquestionnaires" className="link">{t("cancel")}</NavLink>
                </div>         
            </div>     
            : ""           
            }
        </div>
        
    )
}