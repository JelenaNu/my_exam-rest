import React, { useEffect, useState } from 'react'
import { useParams, NavLink } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import questionnairesApi from '../../api/questionnairesApi'
import { useHistory } from 'react-router-dom'


export default () => {
    const { t } = useTranslation("common") 
    const { id } = useParams()
    const [question, setQuestion] = useState()
    const history = useHistory()

    useEffect( () => {
        questionnairesApi.fetchQuestionById(id)
            .then (response => setQuestion(response.data));
    }, [id])

    return (
        <div>
            <h3>{t("deletequestion")}</h3>
            {
                question ? 
                <div className = "card">                            
               
                    <div><span className="prop-name">{t("text")} </span>
                        {question.questionText}
                    </div>
                 
                <div>
                    <button onClick={() => {
                        questionnairesApi.deleteQuestionById(id);
                        history.push(`/questionnaire/${question.questionnaireId}`)}
                        }
                    >{t("submit")}</button>
                    <NavLink to={`/questionnaire/${question.questionnaireId}`}>{t("cancel")}</NavLink>
                </div>         
            </div>     
            : ""           
            }
        </div>
        
    )
}