
INSERT INTO users (username, password, first_name, last_name, email, info, file_name)
VALUES ('albert', '{bcrypt}$2y$12$A7x.2lPxE6YdV8ed6OYbDucRiod32wqMF9JNerE.wq4glQWaIjRnO','Albert', 'Einstein', 'albert@my.com', 'a very clever person', 'anonymous.png');
INSERT INTO users (username, password, first_name, last_name, email, info, file_name)
VALUES ('isaac', '{bcrypt}$2y$12$A7x.2lPxE6YdV8ed6OYbDucRiod32wqMF9JNerE.wq4glQWaIjRnO','Isaac', 'Newton', 'isaac@my.com', 'another very very clever person', 'anonymous.png');
INSERT INTO users (username, password, first_name, last_name, email, info, file_name)
VALUES ('ada', '{bcrypt}$2y$12$A7x.2lPxE6YdV8ed6OYbDucRiod32wqMF9JNerE.wq4glQWaIjRnO','Ada', 'Lovelace', 'ada@my.com', 'another very very clever person', 'anonymous.png');
INSERT INTO users (username, password, first_name, last_name, email, file_name)
VALUES ('admin', '{bcrypt}$2y$12$A7x.2lPxE6YdV8ed6OYbDucRiod32wqMF9JNerE.wq4glQWaIjRnO','Jelena', 'Nu', 'admin@my.com', 'anonymous.png');

INSERT INTO questionnaires (id, theme, description, user_username, lang, accessibility, exam_count, right_answer_count)
VALUES (1, 'Lorem ipsum dolor sit amet consectetur', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad laudantium unde nulla harum eligendi qui, nam fugit ipsum, quia nobis repellat. Ut ea enim debitis ratione a neque minus dolores.', 'isaac', 'english', 'PRIVATE', 10, 20);
INSERT INTO questionnaires (id, theme, description, user_username, lang, accessibility, exam_count, right_answer_count)
VALUES (2, 'At suscipit eos tempora quibusdam eius', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad laudantium unde nulla harum eligendi qui, nam fugit ipsum, quia nobis repellat. Ut ea enim debitis ratione a neque minus dolores.', 'isaac', 'lithuanian', 'PUBLIC', 0, 0);
INSERT INTO questionnaires (id, theme, description, user_username, lang, accessibility, exam_count, right_answer_count)
VALUES (3, 'Ducimus voluptate rerum accusamus inventore', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad laudantium unde nulla harum eligendi qui, nam fugit ipsum, quia nobis repellat. Ut ea enim debitis ratione a neque minus dolores.', 'albert', 'english', 'PUBLIC', 0, 0);

INSERT INTO questions (id, questionnaire_id, question_text)
VALUES (1, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
INSERT INTO questions (id, questionnaire_id, question_text)
VALUES (2, 1, 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.');
INSERT INTO questions (id, questionnaire_id, question_text)
VALUES (3, 2, 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ');
INSERT INTO questions (id, questionnaire_id, question_text)
VALUES (4, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
INSERT INTO questions (id, questionnaire_id, question_text)
VALUES (5, 3, 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ');
INSERT INTO questions (id, questionnaire_id, question_text)
VALUES (6, 3, 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ');
INSERT INTO questions (id, questionnaire_id, question_text)
VALUES (7, 3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');

INSERT INTO options (id, question_id, option_text, is_answer)
VALUES (1, 1, 'yes', true);
INSERT INTO options (id, question_id, option_text, is_answer)
VALUES (2, 1, 'no', false);
INSERT INTO options (id, question_id, option_text, is_answer)
VALUES (3, 2, 'yes', true);
INSERT INTO options (id, question_id, option_text, is_answer)
VALUES (4, 2, 'no', false);
INSERT INTO options (id, question_id, option_text, is_answer)
VALUES (5, 3, 'yes', true);
INSERT INTO options (id, question_id, option_text, is_answer)
VALUES (6, 3, 'no', false);
INSERT INTO options (id, question_id, option_text, is_answer)
VALUES (7, 4, 'yes', true);
INSERT INTO options (id, question_id, option_text, is_answer)
VALUES (8, 4, 'no', false);
INSERT INTO options (id, question_id, option_text, is_answer)
VALUES (9, 5, 'yes', true);
INSERT INTO options (id, question_id, option_text, is_answer)
VALUES (10, 5, 'no', false);
INSERT INTO options (id, question_id, option_text, is_answer)
VALUES (11, 6, 'yes', true);
INSERT INTO options (id, question_id, option_text, is_answer)
VALUES (12, 6, 'no', false);
INSERT INTO options (id, question_id, option_text, is_answer)
VALUES (13, 7, 'yes', true);
INSERT INTO options (id, question_id, option_text, is_answer)
VALUES (14, 7, 'no', false);

INSERT INTO exams (id, user_username, questionnaire_id, right_answers, right_answer_percent)
VALUES (1, 'albert', 1, 2, 90);
INSERT INTO exams (id, user_username, questionnaire_id, right_answers, right_answer_percent)
VALUES (2, 'albert', 3, 0, 0);
INSERT INTO exams (id, user_username, questionnaire_id, right_answers, right_answer_percent)
VALUES (4, 'albert', 3, 2, 70);
INSERT INTO exams (id, user_username, questionnaire_id, right_answers, right_answer_percent)
VALUES (3, 'ada', 3, 3, 100);

INSERT INTO roles(role_id, role) VALUES (1, 'USER');
INSERT INTO roles(role_id, role) VALUES (2, 'ADMIN');

INSERT INTO users_roles(username, role_id) VALUES('albert', 1);
INSERT INTO users_roles(username, role_id) VALUES('isaac', 1);
INSERT INTO users_roles(username, role_id) VALUES('ada', 1);;

INSERT INTO users_roles(username, role_id) VALUES('admin', 1);
INSERT INTO users_roles(username, role_id) VALUES('admin', 2);