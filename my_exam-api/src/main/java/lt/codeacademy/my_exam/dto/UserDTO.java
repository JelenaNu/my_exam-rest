package lt.codeacademy.my_exam.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lt.codeacademy.my_exam.entities.Role;
import lt.codeacademy.my_exam.entities.User;

import java.util.Set;
import java.util.stream.Collectors;

@ApiModel(value = "UserDTO", description = "A user entity without a password included")
@Data
public class UserDTO {
    private String firstName;
    private String lastName;
    private String info;
    private String username;
    private String profilePicture;
    private Set<String> roles;

    public UserDTO(User user) {
        firstName = user.getFirstName();
        lastName = user.getLastName();
        username = user.getUsername();
        info = user.getInfo();
        profilePicture = user.getProfilePicture();
        roles = user.getRoles().stream()
                .map(Role::getRole)
                .collect(Collectors.toSet());

    }
}
