package lt.codeacademy.my_exam.dto;

import lombok.Getter;
import lombok.Setter;
import lt.codeacademy.my_exam.entities.Exam;

import java.time.LocalDateTime;

@Getter
@Setter
public class ExamStatisticsDTO {

    private Long questionnaireId;
    private String theme;
    private String authorsFullName;
    private long rightAnswerCount;
    private int answerPercentage;
    private LocalDateTime takingDate;

    public ExamStatisticsDTO(Exam exam) {
        questionnaireId = exam.getQuestionnaire().getId();
        theme = exam.getQuestionnaire().getTheme();
        rightAnswerCount = exam.getRightAnswersCount();
        this.answerPercentage = exam.getRightAnswerPercent();
        takingDate = exam.getTakingDate();
    }
}
