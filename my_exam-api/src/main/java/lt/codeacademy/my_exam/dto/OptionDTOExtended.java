package lt.codeacademy.my_exam.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lt.codeacademy.my_exam.entities.Option;

@ApiModel(value = "OptionDTOExtended", description = "Option for managing users questionnaires")
@Data
public class OptionDTOExtended extends OptionDTO {

    private boolean answer;

    public OptionDTOExtended(Option option){
        super(option);
        answer = option.getAnswer();
    }

    public OptionDTOExtended() {
    }
}
