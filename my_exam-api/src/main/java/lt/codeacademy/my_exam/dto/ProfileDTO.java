package lt.codeacademy.my_exam.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(value = "ProfileDTO", description = "Users information")
@Data
public class ProfileDTO {

    private String firstName;
    private String lastName;
    private String info;
    private String username;
    private String profilePicture;

}
