package lt.codeacademy.my_exam.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lt.codeacademy.my_exam.entities.Questionnaire;
import lt.codeacademy.my_exam.enums.Accessibility;

import java.time.LocalDateTime;
import java.util.List;

@ApiModel(value = "QuestionnaireDTO", description = "A questionnaire for test")
@Data
public class QuestionnaireDTO {

    protected Long id;
    protected String theme;
    protected String description;
    protected String language;
    protected LocalDateTime creationDate;
    protected String accessibility; // public, private
    protected String authorsUsername;
    protected Long examCount;
    protected Long rightAnswerCount;
    protected List<QuestionDTO> questions;

    public QuestionnaireDTO(Questionnaire questionnaire, List<QuestionDTO> questions) {
        id = questionnaire.getId();
        theme = questionnaire.getTheme();
        description = questionnaire.getDescription();
        language = questionnaire.getLanguage();
        creationDate = questionnaire.getCreationDate();
        accessibility = questionnaire.getAccessibility();
        authorsUsername = questionnaire.getUser().getUsername();
        examCount = questionnaire.getExamCount();
        rightAnswerCount = questionnaire.getRightAnswerCount();
        this.questions = questions;
    }
}
