package lt.codeacademy.my_exam.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lt.codeacademy.my_exam.entities.Option;

@ApiModel(value = "OptionDTO", description = "An option for testin without no answer")
@Data
public class OptionDTO {
    protected Long id;
    protected Long questionId;
    protected String optionText;

    public OptionDTO(Option option) {
        id = option.getId();
        questionId = option.getQuestion().getId();
        optionText = option.getOptionText();
    }

    public OptionDTO() {
    }
}
