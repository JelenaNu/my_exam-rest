package lt.codeacademy.my_exam.dto;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lt.codeacademy.my_exam.entities.Questionnaire;

import java.util.List;

@ApiModel(value = "QuestionnaireDTOExtended", description = "A questionnaire for author with additional statistics")
@Getter
@Setter
public class QuestionnaireDTOExtended extends QuestionnaireDTO {

    private Long passedExams;
    private String rightAnswerPercentage;

    public QuestionnaireDTOExtended(Questionnaire questionnaire,
                                    List<QuestionDTO> questions) {

        super(questionnaire, questions);

        this.passedExams = questionnaire.getExamCount();

        double percentage = 0;
        if (questionnaire.getExamCount() > 0) {
            try {
                percentage = ((double)questionnaire.getRightAnswerCount() / (questions.size() * questionnaire.getExamCount()) ) * 100;
            } catch (Exception ignored) {
            }
        }
        rightAnswerPercentage = String.format("%.2f", percentage);
    }
}
