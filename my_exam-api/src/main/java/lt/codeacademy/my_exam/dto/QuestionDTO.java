package lt.codeacademy.my_exam.dto;

import lombok.Data;
import lt.codeacademy.my_exam.entities.Question;

import java.util.ArrayList;
import java.util.List;

@Data
public class QuestionDTO {

    private Long id;
    private Long questionnaireId;
    private String questionText;

    private List<? extends OptionDTO> options;

    public QuestionDTO(Question question, List<? extends OptionDTO> options) {
        id = question.getId();
        questionnaireId = question.getQuestionnaire().getId();
        questionText = question.getQuestionText();
        this.options = options;
    }

    public QuestionDTO() {
        options = new ArrayList<>();
    }

}

