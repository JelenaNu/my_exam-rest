package lt.codeacademy.my_exam.repositories;

import lt.codeacademy.my_exam.entities.Questionnaire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionnaireRepository extends JpaRepository<Questionnaire, Long> {
    List<Questionnaire> findAllByUserUsername(String username);
    List<Questionnaire> findAllByAccessibility(String accessibility);
    //List<Questionnaire> findAllByUser(User user);
}
