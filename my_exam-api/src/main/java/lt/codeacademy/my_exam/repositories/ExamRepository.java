package lt.codeacademy.my_exam.repositories;

import lt.codeacademy.my_exam.entities.Exam;
import lt.codeacademy.my_exam.entities.Questionnaire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExamRepository extends JpaRepository<Exam, Long> {

    List<Exam> findAllByUserUsername(String username);
    Long countAllByQuestionnaire(Questionnaire questionnaire);
    List<Exam> findAllByQuestionnaire(Questionnaire questionnaire);

    @Query(value = "SELECT SUM(right_answers) FROM exams WHERE questionnaire_id=?1", nativeQuery = true)
    Long sumAnswersByQuestionnaire(Long questionnaireId);


}
