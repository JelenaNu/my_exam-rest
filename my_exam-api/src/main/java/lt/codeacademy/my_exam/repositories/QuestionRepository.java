package lt.codeacademy.my_exam.repositories;

import lt.codeacademy.my_exam.entities.Question;
import lt.codeacademy.my_exam.entities.Questionnaire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
    int countAllByQuestionnaire(Questionnaire questionnaire);
    List<Question> findAllByQuestionnaire(Questionnaire questionnaire);
}
