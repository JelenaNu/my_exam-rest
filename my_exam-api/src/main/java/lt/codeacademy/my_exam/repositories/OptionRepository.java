package lt.codeacademy.my_exam.repositories;

import lt.codeacademy.my_exam.entities.Option;
import lt.codeacademy.my_exam.entities.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OptionRepository extends JpaRepository<Option, Long> {
    List<Option> findAllByQuestion(Question question);
}
