package lt.codeacademy.my_exam.services;

import lt.codeacademy.my_exam.entities.User;
import lt.codeacademy.my_exam.repositories.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;
    private final FileStorageService fileStorageService;

    public UserDetailsServiceImpl(UserRepository userRepository,
                                  FileStorageService fileStorageService) {
        this.userRepository = userRepository;
        this.fileStorageService = fileStorageService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findUserByUsername(username)
                .orElseThrow( () -> new UsernameNotFoundException("No user found by name: " + username));
    }

    public ResponseEntity<?> updateUser(User user, MultipartFile file) {
        if (file != null) {
            user.setProfilePicture(file.getOriginalFilename());
            fileStorageService.storeFile(file);
        }
        userRepository.save(user);
        return ResponseEntity.ok().build();
    }
}
