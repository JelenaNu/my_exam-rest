package lt.codeacademy.my_exam.services;

import lt.codeacademy.my_exam.entities.*;
import lt.codeacademy.my_exam.enums.Accessibility;
import lt.codeacademy.my_exam.exceptions.ResourceNotAvailableException;
import lt.codeacademy.my_exam.exceptions.ResourceNotFoundException;
import lt.codeacademy.my_exam.repositories.ExamRepository;
import lt.codeacademy.my_exam.repositories.OptionRepository;
import lt.codeacademy.my_exam.repositories.QuestionRepository;
import lt.codeacademy.my_exam.repositories.QuestionnaireRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class QuestionnaireService {

    private final QuestionnaireRepository questionnaireRepository;
    private final QuestionRepository questionRepository;
    private final OptionRepository optionRepository;
    private final ExamRepository examRepository;

    public QuestionnaireService(QuestionnaireRepository questionnaireRepository,
                                QuestionRepository questionRepository,
                                OptionRepository optionRepository,
                                ExamRepository examRepository
    ) {
        this.questionnaireRepository = questionnaireRepository;
        this.questionRepository = questionRepository;
        this.optionRepository = optionRepository;
        this.examRepository = examRepository;
    }


    public List<Option> getOptionsByQuestion(Question question) {
        return optionRepository.findAllByQuestion(question);
    }

    public List<Questionnaire> getQuestionnairesByUsername(String username) {
        return questionnaireRepository.findAllByUserUsername(username);
    }

  /*  public int getQuestionCount(Questionnaire questionnaire) {
        return questionRepository.countAllByQuestionnaire(questionnaire);
    }*/

    public List<Question> getQuestionsByQuestionnaire(Questionnaire questionnaire) {
        return questionRepository.findAllByQuestionnaire(questionnaire);
    }

    public Questionnaire saveOrUpdateQuestionnaire(Questionnaire questionnaire) {
        return questionnaireRepository.save(questionnaire);
    }

    public Questionnaire getQuestionnaireById(Long id) {
        return questionnaireRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("questionnaire with id " + id + " not found"));
    }

    public Questionnaire getAvailableQuestionnaireById(User user, Long id) {
        Questionnaire questionnaire = getQuestionnaireById(id);
        if (questionnaire.getAccessibility().equals(Accessibility.PUBLIC.toString()) ||
            questionnaire.getUser().equals(user)) {
            return questionnaire;
        } else
            throw new ResourceNotAvailableException("Questionnaire with id: " + id + " is not available for user " + user.getUsername());
    }

    public Question getAvailableQuestionById(User user, Long id) {
       Question question = questionRepository.findById(id).get();
       if (question.getQuestionnaire().getUser().equals(user)) {
           return question;
        } else {
            throw new ResourceNotFoundException("question with id " + id + " not found");
       }
    }

    public ResponseEntity<?> deleteQuestion(User user, Long id) {
        return questionRepository.findById(id).map(question -> {
            if(question.getQuestionnaire().getUser().equals(user)) {
                questionRepository.delete(question);
                return ResponseEntity.ok().build();
            } else {
                return ResponseEntity.badRequest().build();
            }
        }).orElseThrow(() -> new ResourceNotFoundException("Question not found"));
    }

    public ResponseEntity<?> deleteQuestionnaire(User user, Long id) {
        Questionnaire questionnaire = questionnaireRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Questionnaire not found"));
        if (questionnaire.getUser().equals(user) || hasRole(user.getRoles(), "ADMIN")) {
            questionnaireRepository.delete(questionnaire);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    private boolean hasRole(Set<Role> roles, String role) {
        for (Role r : roles) {
            if (r.getRole().equals(role)) {
                return true;
            }
        }
        return false;
    }

    public ResponseEntity<?> deleteOption(User user, Long id) {
        return optionRepository.findById(id).map(option -> {
            if(option.getQuestion().getQuestionnaire().getUser().equals(user)) {
                optionRepository.delete(option);
                return ResponseEntity.ok().build();
            } else {
                return ResponseEntity.badRequest().build();
            }
        }).orElseThrow(() -> new ResourceNotFoundException("Option not found"));
    }

    public Question saveOrUpdateQuestion(Question question) {
        return questionRepository.save(question);
    }

    public Question getQuestionById(Long id) {
        return questionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("question with id " + id + " not found"));
    }

    public Option saveOrUpdateOption(Option option) {
        return optionRepository.save(option);
    }

    public Option getOptionById(Long id) {
        return optionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("option with id " + id + " not found"));
    }

    public List<Questionnaire> getPublicQuestionnaires() {
        return questionnaireRepository.findAllByAccessibility(Accessibility.PUBLIC.toString());
    }
}

