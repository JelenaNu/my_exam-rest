package lt.codeacademy.my_exam.services;

import lt.codeacademy.my_exam.entities.*;
import lt.codeacademy.my_exam.enums.Accessibility;
import lt.codeacademy.my_exam.repositories.ExamRepository;
import lt.codeacademy.my_exam.repositories.OptionRepository;
import lt.codeacademy.my_exam.repositories.QuestionRepository;
import lt.codeacademy.my_exam.repositories.QuestionnaireRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExamService {

    private final ExamRepository examRepository;
    private final QuestionRepository questionRepository;
    private final OptionRepository optionRepository;
    private final QuestionnaireRepository questionnaireRepository;

    public ExamService(ExamRepository examRepository,
                       QuestionnaireRepository questionnaireRepository,
                       QuestionRepository questionRepository,
                       OptionRepository optionRepository) {
        this.examRepository = examRepository;
        this.questionRepository = questionRepository;
        this.optionRepository = optionRepository;
        this.questionnaireRepository = questionnaireRepository;
    }

    public List<Exam> getExamsByUsername(String username) {
        return examRepository.findAllByUserUsername(username);
    }

    public long getExamCount(Questionnaire questionnaire) {
        return examRepository.countAllByQuestionnaire(questionnaire);
    }

    public long getAllExamAnswerCount(Questionnaire questionnaire) {
        return examRepository.sumAnswersByQuestionnaire(questionnaire.getId());
    }

    public boolean isAvailableForExam(User user, Questionnaire questionnaire) {
        return (!questionnaire.getUser().equals(user)
                && questionnaire.getAccessibility().equals(Accessibility.PUBLIC.toString()));
    }

    public void takeExam(User user, List<Long> selectedOptions) {
        Exam exam = new Exam();
        Questionnaire questionnaire = optionRepository.findById(selectedOptions.get(0)).get().getQuestion().getQuestionnaire();

        exam.setUser(user);
        exam.setRightAnswersCount(selectedOptions.stream()
                .map(selectedOption -> optionRepository.findById(selectedOption).get())
                .filter(Option::getAnswer).count());
        exam.setQuestionnaire(questionnaire);

        int questionCount = questionRepository.countAllByQuestionnaire(exam.getQuestionnaire());
        exam.setRightAnswerPercent((int) ((double) exam.getRightAnswersCount() / questionCount * 100));
        examRepository.save(exam);

        questionnaire.setExamCount(questionnaire.getExamCount() + 1);
        questionnaire.setRightAnswerCount(questionnaire.getRightAnswerCount() + exam.getRightAnswersCount());
        questionnaireRepository.save(questionnaire);
    }
}
