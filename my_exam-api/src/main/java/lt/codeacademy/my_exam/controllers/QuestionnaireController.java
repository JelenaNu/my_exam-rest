package lt.codeacademy.my_exam.controllers;

import io.swagger.annotations.ApiOperation;
import lt.codeacademy.my_exam.dto.*;
import lt.codeacademy.my_exam.entities.Option;
import lt.codeacademy.my_exam.entities.Question;
import lt.codeacademy.my_exam.entities.Questionnaire;
import lt.codeacademy.my_exam.entities.User;
import lt.codeacademy.my_exam.services.QuestionnaireService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class QuestionnaireController {

    protected final QuestionnaireService questionnaireService;
    private final DTOCreationHelper dtoCreationHelper;

    public QuestionnaireController(QuestionnaireService questionnaireService,
                                   DTOCreationHelper dtoCreationHelper) {
        this.questionnaireService = questionnaireService;
        this.dtoCreationHelper = dtoCreationHelper;
    }

    @GetMapping("/option/{id}/delete")
    public ResponseEntity<?> deleteOption(@AuthenticationPrincipal User user,
                                          @PathVariable Long id) {
        return questionnaireService.deleteOption(user, id);
    }

    @GetMapping("/question/{id}/delete")
    public ResponseEntity<?> deleteQuestion(@AuthenticationPrincipal User user,
                                            @PathVariable Long id) {
        return questionnaireService.deleteQuestion(user, id);
    }

    @GetMapping("/questionnaire/{id}/delete")
    public ResponseEntity<?> deleteQuestionnaire(@AuthenticationPrincipal User user,
                                                 @PathVariable Long id) {
        return questionnaireService.deleteQuestionnaire(user, id);
    }

    @ApiOperation(value = "get current user questionnaires for edit")
    @GetMapping("/myquestionnaires")
    public List<QuestionnaireDTOExtended> getMyQuestionnaires(@AuthenticationPrincipal User user) {
        List<Questionnaire> questionnaires = questionnaireService.getQuestionnairesByUsername(user.getUsername());
        return dtoCreationHelper.createQuestionnaireDTOList(questionnaires);
    }

    @ApiOperation(value = "creates or saves a questionnaire")
    @PostMapping("/questionnaire")
    public Questionnaire saveQuestionnaire(@AuthenticationPrincipal User user,
                                           @RequestBody Questionnaire questionnaire) {
        questionnaire.setUser(user);
        return questionnaireService.saveOrUpdateQuestionnaire(questionnaire);
    }

    @ApiOperation(value = "returns a questionnaire to its owner")
    @GetMapping("/questionnaire/{id}")
    public QuestionnaireDTOExtended getQuestionnaireById(@AuthenticationPrincipal User user,
                                                         @PathVariable Long id) {
        Questionnaire questionnaire = questionnaireService.getAvailableQuestionnaireById(user, id);
        return dtoCreationHelper.createQuestionnaireDTO(questionnaire);
    }

    @GetMapping("/question/{id}")
    public QuestionDTO getQuestionById(@AuthenticationPrincipal User user,
                                       @PathVariable Long id) {
        Question question = questionnaireService.getAvailableQuestionById(user, id);
        return new QuestionDTO(question, dtoCreationHelper.createOptionDTOExtendedList(question));
    }

    @GetMapping("/option/{id}")
    public OptionDTOExtended getOptionById(@PathVariable Long id) {
        Option option = questionnaireService.getOptionById(id);
        return new OptionDTOExtended(option);
    }

    @PostMapping("/question")
    public Question saveQuestion(@RequestBody QuestionDTO question) {
        return questionnaireService.saveOrUpdateQuestion(dtoCreationHelper.getQuestionFromDTO(question));
    }

    @PostMapping("/option")
    public Option saveOption(@RequestBody OptionDTOExtended option) {
        return questionnaireService.saveOrUpdateOption(dtoCreationHelper.getOptionFromDTO(option));
    }

    @ApiOperation(value = "returns a profile info to owner")
    @GetMapping("/myprofile")
    public ProfileDTO getMyProfile(@AuthenticationPrincipal User user) {
        return dtoCreationHelper.createProfileFromUser(user);
    }

    @ApiOperation(value = "returns all public questionnaires for test")
    @GetMapping("/publicquestionnaires")
    public List<QuestionnaireDTO> getPublicQuestionnaires(@AuthenticationPrincipal User user) {
        return dtoCreationHelper.getPublicQuestionnaires(user.getUsername());
    }
}
