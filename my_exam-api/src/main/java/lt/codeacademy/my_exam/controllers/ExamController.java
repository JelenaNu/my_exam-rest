package lt.codeacademy.my_exam.controllers;

import io.swagger.annotations.ApiOperation;
import lt.codeacademy.my_exam.dto.ExamStatisticsDTO;
import lt.codeacademy.my_exam.dto.QuestionDTO;
import lt.codeacademy.my_exam.entities.Exam;
import lt.codeacademy.my_exam.entities.Questionnaire;
import lt.codeacademy.my_exam.entities.User;
import lt.codeacademy.my_exam.services.ExamService;
import lt.codeacademy.my_exam.services.QuestionnaireService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ExamController {

    private final QuestionnaireService questionnaireService;
    private final DTOCreationHelper dtoCreationHelper;
    private final ExamService examService;

    public ExamController(QuestionnaireService questionnaireService,
                          ExamService examService,
                          DTOCreationHelper dtoCreationHelper) {
        this.questionnaireService = questionnaireService;
        this.examService = examService;
        this.dtoCreationHelper = dtoCreationHelper;
    }

    @ApiOperation(value = "returns examination statistics")
    @GetMapping("/exams")
    public List<ExamStatisticsDTO> getExamsByUsername(@AuthenticationPrincipal User user) {
        List<Exam> exams = examService.getExamsByUsername(user.getUsername());
        return dtoCreationHelper.createExamDTOs(exams);
    }

    @ApiOperation(value = "returns list of questions for the test")
    @GetMapping("/exam/questions/{questionnaireId}")
    public List<QuestionDTO> getQuestionsForExamByQuestionnaireId(
                                                    @AuthenticationPrincipal User user,
                                                    @PathVariable Long questionnaireId){
        Questionnaire questionnaire = questionnaireService.getQuestionnaireById(questionnaireId);
        if (examService.isAvailableForExam(user, questionnaire)) {
            return dtoCreationHelper.createQuestionsForExam(questionnaire);
        }
        return new ArrayList<>();
    }

    @ApiOperation(value = "creates exam entity")
    @PostMapping("/exam")
    public void takeExam(@AuthenticationPrincipal User user,
                                @RequestBody List<Long> selectedOptions) {
        if (selectedOptions.size() > 0) {
            examService.takeExam(user, selectedOptions);
        }
    }
}
