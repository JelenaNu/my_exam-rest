package lt.codeacademy.my_exam.controllers;

import lt.codeacademy.my_exam.dto.UserDTO;
import lt.codeacademy.my_exam.entities.User;
import lt.codeacademy.my_exam.exceptions.ResourceNotAvailableException;
import lt.codeacademy.my_exam.services.UserDetailsServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserDetailsServiceImpl userDetailsService;

    public UserController(UserDetailsServiceImpl userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @GetMapping
    public UserDTO getUser(@AuthenticationPrincipal User user) {
        return new UserDTO(user);
    }

    @PostMapping("/update")
    public ResponseEntity<?> updateUser(@AuthenticationPrincipal User user,
                                     @RequestParam(name = "file", required = false) MultipartFile file,
                                     @RequestParam(name = "username") String username,
                                     @RequestParam(name = "firstName", required = false) String firstName,
                                     @RequestParam(name = "lastName", required = false) String lastName,
                                     @RequestParam(name = "info", required = false) String info) {
        if (username.equals(user.getUsername())) {
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setInfo(info);
            if (file != null) {
                user.setProfilePicture(file.getOriginalFilename());
            }
        } else {
            return ResponseEntity.badRequest().build();
        }
        return userDetailsService.updateUser(user, file);
    }

}
