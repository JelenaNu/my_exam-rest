package lt.codeacademy.my_exam.controllers;

import lt.codeacademy.my_exam.dto.*;
import lt.codeacademy.my_exam.entities.*;
import lt.codeacademy.my_exam.services.ExamService;
import lt.codeacademy.my_exam.services.QuestionnaireService;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DTOCreationHelper {

    private final QuestionnaireService questionnaireService;
    private final ExamService examService;

    public DTOCreationHelper(QuestionnaireService questionnaireService,
                             ExamService examService) {
        this.questionnaireService = questionnaireService;
        this.examService = examService;
    }

    protected List<ExamStatisticsDTO> createExamDTOs(List<Exam> exams) {
        return exams.stream()
                .map(ExamStatisticsDTO::new)
                .collect(Collectors.toList());
        /*List<ExamStatisticsDTO> resultList = new ArrayList<>();
        for (Exam exam: exams) {
            resultList.add(new ExamStatisticsDTO(exam));
        }
        return resultList;*/
    }

    protected List<QuestionnaireDTOExtended> createQuestionnaireDTOList(List<Questionnaire> questionnaires) {
        return questionnaires.stream()
                .map(this::createQuestionnaireDTO)
                .collect(Collectors.toList());
    }

    protected List<QuestionDTO> createQuestionDTOList(Questionnaire questionnaire) {
        List<Question> questions = questionnaireService.getQuestionsByQuestionnaire(questionnaire);
        return questions.stream()
                .map(question -> new QuestionDTO(question, createOptionDTOExtendedList(question)))
                .collect(Collectors.toList());
    }

    protected QuestionnaireDTOExtended createQuestionnaireDTO(Questionnaire questionnaire) {
        long examCount = examService.getExamCount(questionnaire);
        long rightAnswers = 0;
        if (examCount > 0) {
            rightAnswers = examService.getAllExamAnswerCount(questionnaire);
        }
        return new QuestionnaireDTOExtended(questionnaire, createQuestionDTOList(questionnaire));
    }
    
    protected List<OptionDTOExtended> createOptionDTOExtendedList(Question question) {
        return questionnaireService.getOptionsByQuestion(question).stream()
                .map(OptionDTOExtended::new)
                .collect(Collectors.toList());
    }

    protected Question getQuestionFromDTO(QuestionDTO questionDTO) {
        Question question = new Question();
        Questionnaire questionnaire = questionnaireService.getQuestionnaireById(questionDTO.getQuestionnaireId());
        question.setId(questionDTO.getId());
        question.setQuestionnaire(questionnaire);
        question.setQuestionText(questionDTO.getQuestionText());
        return question;
    }

    public Option getOptionFromDTO(OptionDTOExtended optionDTOExtended) {
        Option option = new Option();
        Question question = questionnaireService.getQuestionById(optionDTOExtended.getQuestionId());
        option.setId(optionDTOExtended.getId());
        option.setQuestion(question);
        option.setOptionText(optionDTOExtended.getOptionText());
        option.setAnswer(optionDTOExtended.isAnswer());
        return option;
    }

    protected ProfileDTO createProfileFromUser(User user) {
        ProfileDTO profile = new ProfileDTO();
        profile.setFirstName(user.getFirstName());
        profile.setLastName(user.getLastName());
        profile.setInfo(user.getInfo());
        profile.setProfilePicture(user.getProfilePicture());
        profile.setUsername(user.getUsername());
        return profile;
    }

    protected List<QuestionnaireDTO> getPublicQuestionnaires(String username) {
        return questionnaireService.getPublicQuestionnaires().stream()
                .filter(questionnaire -> !questionnaire.getUser().getUsername().equals(username))
                .map(questionnaire -> new QuestionnaireDTO(questionnaire, new ArrayList<>()))
                .collect(Collectors.toList());
    }

    protected List<OptionDTO> createOptionDTOList(Question question) {
        return questionnaireService.getOptionsByQuestion(question).stream()
                .map(OptionDTO::new)
                .collect(Collectors.toList());
    }

    protected List<QuestionDTO> createQuestionsForExam(Questionnaire questionnaire) {
        return questionnaireService.getQuestionsByQuestionnaire(questionnaire)
                .stream()
                .map(question -> new QuestionDTO(question, createOptionDTOList(question)))
                .collect(Collectors.toList());
    }
}


