package lt.codeacademy.my_exam.entities;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lt.codeacademy.my_exam.enums.Accessibility;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "questionnaires")
public class Questionnaire {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    @Size(min = 3, max = 100)
    private String theme;

    @NotEmpty
    @Size(min = 50, max = 250)
    private String description;

    @Column(name = "lang")
    @NotEmpty
    private String language;

    @CreationTimestamp
    @ColumnDefault("CURRENT_TIMESTAMP")
    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    @NotEmpty
    @Column
    private String accessibility; // public, private, by invitation

    @Column(name = "exam_count", columnDefinition = "int default 0")
    private Long examCount;

    @Column(name = "right_answer_count", columnDefinition = "int default 0")
    private Long rightAnswerCount;

    @ManyToOne
    @JoinColumn(name = "user_username")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;
}
