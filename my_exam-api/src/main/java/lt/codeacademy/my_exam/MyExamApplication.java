package lt.codeacademy.my_exam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyExamApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyExamApplication.class, args);
	}

}
