package lt.codeacademy.my_exam.exceptions;

public class ResourceNotAvailableException extends RuntimeException{
    public ResourceNotAvailableException(String message) {
        super(message);
    }
}
