**my_exam Questionnaires**

This program was made as an Anvanced Java course project (https://www.codeacademy.lt/)
The program is an instrument for taking exams and for making them, too. It alows you to create your own tests, questions and options for them, take exams, which was made by other users, also manage your user profile.
At this moment my_exam supports these interface languages: Russian, Lithuanian and English.

--------------------------------------------------------------------------

For installing and running the program, you need to have on your computer the following software:

    JRE (Java Runtime Environment)
    Node.js

--------------------------------------------------------------------------

**Installing**

Open a terminal, go to the directory with the downloaded file (location: my_exam-api/target/)

    my_exam-0.0.1-SNAPSHOT.jar 

Type the command and run a server

    java -Dspring.profiles.active=prod -jar my_exam-0.0.1-SNAPSHOT.jar 

Go to the folder my_exam-ui, run the following commands:

    npm install
    npm start

The commands have to start a ui. Go to your browser. The program is available on

    http://localhost:3000/


